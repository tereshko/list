#include <iostream>

struct Node
	{
	int value = 0;
	Node* next;
	};

Node* createNode (int data) {
	size_t size_of_node = sizeof(Node);
	Node* newNode = (Node*)malloc(size_of_node);
	newNode->value = data;
	newNode->next = nullptr;
	return newNode;
}

Node* pushBack( Node* firstNode){

	int value = 0;
	std::cout << "\nEnter new value for new node: ";
	std::cin >> value;

	Node* newUserNode;
	newUserNode = createNode( value);

	if( firstNode == NULL){
		firstNode = newUserNode;
	} else{
		while( firstNode->next != nullptr){
			firstNode = firstNode->next;
		}
		firstNode->next = newUserNode;
	}
	return firstNode;
}

int choseAction(){

	bool isAnswerCorrect = false;
	int action = 0;
	while (!isAnswerCorrect) {

		std::cout << "1. Print nodes with id\n";
		std::cout << "2. Add new node to the end\n";
		std::cout << "3. Put new value by ID\n";
		std::cout << "4. Delete by ID\n";
		std::cout << "5. Count\n";
		std::cout << "6. Exit\n";
		std::cout << "Select action: ";
		std::cin >> action;

		isAnswerCorrect =( 0 < action) && ( action <= 6);
	}
	std::cout << "\n";

	return action;
}

int countOfNodes( Node* firstNode) {
	int nodesCount = 0;
	if( firstNode == NULL){
		std::cout << "The list is empty\n";
	} else {
		Node* currentNode;
		currentNode = firstNode;
		if( currentNode->next == nullptr){
			nodesCount = 1;
		} else{
			while(currentNode->next != nullptr) {
				nodesCount++;
				currentNode = currentNode->next;
			}
		}
	}
	return nodesCount;
}

Node* putNewNodeById( Node* firstNode){

	int newValue =0;
	std::cout << "Please insert new value: ";
	std::cin >> newValue;

	Node* nodeForPut;
	nodeForPut = createNode(newValue);

	Node* headNode;

	if( firstNode == NULL){
		headNode = nodeForPut;
	} else {
		int nodesCount = countOfNodes( firstNode);

		bool isIndexCorrect = false;
		int placeForPut = 0;
		while(!isIndexCorrect) {
			std::cout << "Where i should put your value: ";
			std::cin >> placeForPut;

			if(( 0 <= placeForPut) && (placeForPut <= nodesCount)) {
				isIndexCorrect = true;
			} else {
				std::cout << "Sorry, incorrect place\n";
			}
		}

		Node* currentNode;
		currentNode = firstNode;

		if( placeForPut == 0) {
			nodeForPut->next = currentNode;
			headNode = nodeForPut;
		} else {
			headNode = currentNode;
			for (int i = 1; i < placeForPut; i++){
				currentNode = currentNode->next;
			}
			nodeForPut->next = currentNode->next;
			currentNode->next = nodeForPut;
		}
	}

	return headNode;
}

int requestNodeId( Node* firstNode){
	int nodesCount = countOfNodes( firstNode);
	bool isIdCorrect = false;
	int nodeId = 0;

	while(!isIdCorrect) {
		std::cout << "Id for delete: ";
		std::cin >> nodeId;

		if(( 0 <= nodeId) && ( nodeId < nodesCount)) {
			isIdCorrect = true;
		} else {
			std::cout << "Sorry, incorrect Id\n";
		}
	}
	return nodeId;
}

Node* removeNodeById(Node* firstNode){

	Node* newHead;

	if( firstNode == NULL){
		newHead = NULL;
	} else {
		int nodeId = 0;
		nodeId = requestNodeId( firstNode);

		if (nodeId == 0) {
			newHead = firstNode->next;
			free( firstNode);
		} else {
			newHead = firstNode;
			Node* nodeForDelete;
			for( int i = 1; i < nodeId; i++) {
				firstNode = firstNode->next;
			}
			nodeForDelete = firstNode;
			nodeForDelete = nodeForDelete->next;
			firstNode->next = nodeForDelete->next;
			free( nodeForDelete);
		}
	}
	return newHead;
}

void printNodeWithId( Node* firstNode){
	int id = 0;
	std::cout << "\n";

	if( firstNode == NULL){
		std::cout << "The list is empty\n";
	} else{
		Node* currentNode;
		currentNode = firstNode;
		while(currentNode->next != nullptr) {
			std::cout << "ID: " << id << " value: ";
			std::cout << currentNode->value << ";\n";
			currentNode = currentNode->next;
			id++;
		}
		std::cout << "ID: " << id << " value: " << currentNode->value << ";\n";
	}
}

void printCountOfNodes( Node* firstNode) {
	if( firstNode == NULL){
		std::cout << "0 nodes\n";
	} else {
		int nodesCount = countOfNodes( firstNode);
		std::cout << "\nNodes: " << nodesCount << "\n";
	}
}

int main (int argc, char** argv) {

	Node* firstNode;
	firstNode = NULL;

	bool isExitRequested = false;

	while( !isExitRequested){
		std::cout << "\n";
		int action = 0;
		action = choseAction();

		switch (action) {
		case 1:
			printNodeWithId( firstNode);
			break;
		case 2:
			printNodeWithId( firstNode);
			firstNode = pushBack( firstNode);
			printNodeWithId( firstNode);
			break;
		case 3:
			printNodeWithId( firstNode);
			firstNode = putNewNodeById( firstNode);
			printNodeWithId( firstNode);
			break;
		case 4:
			printNodeWithId( firstNode);
			firstNode = removeNodeById( firstNode);
			break;
		case 5:
			printCountOfNodes( firstNode);
			break;
		case 6:
			isExitRequested = true;
			break;
		default:
			std::cout << "Incorrect selection\n";
			break;
		}
	}

}